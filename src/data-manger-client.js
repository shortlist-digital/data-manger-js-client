var DataMangerClient = (function(window, $, CryptoJS) {

  var data_manger_url = 'http://data-manger.io.shortlistmedia.co.uk/data-record';
  var data_manger_url_unique = 'http://data-manger.io.shortlistmedia.co.uk/data-record/unique';

  /**
   * DataMangerClient
   *
   * Example usage:
   *
   *   var client = new DataMangerClient(DATA_MANGER_APP_KEY, DATA_MANGER_APP_SECRET, {emailAddress: 'test@example.com'},[]);
   *   client.post().then(function() {
   *     alert('Hurrah');
   *   }, function() {
   *     alert('Oh dear');
   *   });
   *
   * @constructor
   * @this {DataMangerClient}
   * @param {string} appKey The Data Manger app key
   * @param {string} appSecret The Data Manger app secret
   * @param {object} [payload] The data to send to Data Manger
   * @param {array} [compoundKeyFields] An array of fields to form a compound key
   */
  function DataMangerClient(appKey, appSecret, payload, compoundKeyFields) {

    // Automatically set payload if provided
    if (typeof payload !== 'undefined') {
      setPayload(payload);
    }

    // Automatically set unique compounds if provided
    if (typeof compoundKeyFields !== 'undefined') {
      setCompoundKeyFields(compoundKeyFields);
    }

    /**
     * Setter for payload
     * Automatically prepares nonce and signature.
     * Chainable.
     *
     * @param {object} newPayload The data to send to Data Manger
     * @throws {Error} Payload must be an object
     * @returns {DataMangerClient}
     */
    function setPayload(newPayload) {
      if (typeof newPayload !== 'object') {
        throw new Error('Payload is not an object');
      }

      payload = newPayload;

      preparePayload();

      return this;
    }

    /**
     * Setter for compoundKeyFields
     *
     * @param {array} The array list of compound key fields 
     * @throws {Error} Payload must be an array
     * @returns {DataMangerClient}
     */
    function setCompoundKeyFields(newCompoundKeyFields) {
      if (!$.isArray(newCompoundKeyFields)) {
        throw new Error('Compound key fields must be provided as an array');
      }

      payload.compoundKeyFields = newCompoundKeyFields.join();

      return this;
    }

    /**
     * Prepare the payload for sending
     * Populates nonce and signature.
     */
    function preparePayload() {
      payload.appKey    = appKey;
      payload.nonce     = generateNonce();
      payload.signature = generateSignature(appSecret, payload.nonce);
    }

    /**
     * Execute the request
     *
     * @returns {Promise} Promise
     */
    function post() {
      if (payload.compoundKeyFields)  {
        data_manger_url = data_manger_unique_url;
      }
      return $.ajax({
        type: 'POST',
        url: data_manger_url,
        contentType: 'text/plain',
        data: JSON.stringify(payload)
      });
    }

    /**
     * Public API
     */
    this.setPayload = setPayload;
    this.post       = post;
  }

  /**
   * Generate nonce
   *
   * @returns {string} Nonce as string
   */
  function generateNonce() {
    return Math.round(Math.random() * 10000000).toString();
  }

  /**
   * Generate signature for the request
   *
   * @param {string} appSecret The Data Manger app secret
   * @param {string} nonce The nonce that will be sent with the payload
   * @returns {string} Signature for request
   */
  function generateSignature(appSecret, nonce) {
    var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA1, appSecret);
    hmac.update(nonce);

    return hmac.finalize().toString(CryptoJS.enc.Hex);
  }

  return DataMangerClient;

})(window, $, CryptoJS);
