Data Manger JS Client
=====================

## Requirements

Until this is made into a CommonJS module (etc.), it requires global jQuery (`$`) and [CryptoJS](https://code.google.com/p/crypto-js/) (`CryptoJS`).

## CORS Support

The POST request to Data Manger is made in a manner compatible with the [jQuery.XDomainRequest.js](https://github.com/MoonScript/jQuery-ajaxTransport-XDomainRequest/blob/master/jQuery.XDomainRequest.js) library, so include that if you need to support IE8.

## Usage

Instantiate your client with an appKey, appSecret, and (optionally) a payload object

```js
var client, payload;

payload = {emailAddress: 'test@example.com'};

client = new DataMangerClient(DATA_MANGER_APP_KEY, DATA_MANGER_APP_SECRET, payload);
```

If you provide a `payload`, `client.setPayload(payload)` is called internally.


### client.setPayload(payload)

If you don’t set a payload when you instantiate the client, you can use `client.setPayload(payload)` yourself. A new [nonce](http://en.wikipedia.org/wiki/Cryptographic_nonce) and signature will be automatically generated each time this is called.

```js
client.setPayload({foo: 'bar'});
```


### client.post()

Post the payload to Data Manger, and receive a Promise in return. The Promise is the direct result of `$.ajax()`, so your success and error handlers can accept useful status/error parameters.

```js
client.post()
  .then(function(data, textStatus, jqXHR) {
    alert('Hurrah');
  }, function(jqXHR, textStatus, errorThrown) {
    alert('Oh dear');
  });
```


## Other Notes

`client.setPayload(payLoad)` is chainable in case you want to populate and send the data with slightly less typing.

```js
client.setPayload({foo: 'bar'}).post(function() {
  alert('Hurrah');
});
```


## TODO

-   Package for Browserify etc., and handle dependencies properly.
